// $(function () {
//     $("#takePhoto").on('click', function () {
//         console.log("TakePhoto1");
//         // window.alert("TakePhoto");
//         var pic = "D:\\ROBOTic\\OpenVINO\\object_detection_demo-master\\dataset\\Jeremy.png"
//         document.getElementById('face_img').src = pic//.replace('90x90', '225x225');
//         document.getElementById('add_face_div').style.display = 'block';
//     });
//     $('#add_face_submit').on('click', function () {
//         var x = document.getElementById('add_face_div')
//         if (x.style.display === "none") {
//             x.style.display = "block";
//         } else {
//             x.style.display = "none";
//         }
//     });
// });
$(function () {
    var detection_cmd = ['async', 'sync', 'object_detection',
        'age_gender_detection', 'face_detection',
        'emotions_detection', 'head_pose_detection',
        'facial_landmarks_detection', 'facial_recognition'];
    var flip_cmd = ['flip'];
    var add_face_cmd = ['take_photo', 'add_face_submit'];
    var url = "";
    $('.btn').on('click', function () {
        var command = JSON.stringify({ "command": $('#' + $(this).attr('id')).val() });
        if (JSON.parse(command).command == "") {
            var command = JSON.stringify({ "command": $(this).find('input').val() });
        }

        if (detection_cmd.includes(JSON.parse(command).command)) {
            url = '/detection';
            post(url, command);
        }
        if (flip_cmd.includes(JSON.parse(command).command)) {
            url = '/flip';
            post(url, command);
        }
        if (add_face_cmd.includes(JSON.parse(command).command)) {
            if ("take_photo" == JSON.parse(command).command) {
                $("#face_img").removeAttr('src');
                document.getElementById('add_face_div').style.display = 'none';
            }
            if ("add_face_submit" == JSON.parse(command).command){
                if ($("#face_name").val() == ""){             
                    window.alert("Need to key your name");
                    return;
                }
            }
            url = '/add_face';
            var command = JSON.stringify({ "command": JSON.parse(command).command,
                "face_name": $('#face_name').val() });
            post(url, command);

        }
    });
    function post(url, command) {
        $.ajax({
            type: 'POST',
            url: url,
            data: command,
            contentType: 'application/json',
            timeout: 10000
        }).done(function (data) {
            var sent_cmd = JSON.parse(command).command;
            var is_async_mode = JSON.parse(data.ResultSet).is_async_mode;
            var flip_code = JSON.parse(data.ResultSet).flip_code;
            var is_obj_det = JSON.parse(data.ResultSet).is_object_detection;
            var is_face_det = JSON.parse(data.ResultSet).is_face_detection;
            var is_ag_det = JSON.parse(data.ResultSet).is_age_gender_detection;
            var is_em_det = JSON.parse(data.ResultSet).is_emotions_detection;
            var is_hp_det = JSON.parse(data.ResultSet).is_head_pose_detection;
            var is_lm_det = JSON.parse(data.ResultSet).is_facial_landmarks_detection;
            var is_fr = JSON.parse(data.ResultSet).is_facial_recognition;
            var face_count = JSON.parse(data.ResultSet).face_count;
            console.log(data.ResultSet);
            $("#res").text("obj:" + is_obj_det + " face:" + is_face_det + " ag:" + is_ag_det + " em:" + is_em_det + " hp:" + is_hp_det + " lm:" + is_lm_det + " fr:" + is_fr);

            if ("take_photo" == sent_cmd) {
                if (face_count == 0){
                    window.alert("No face was detected");
                    return;
                }else{
                    document.getElementById('add_face_div').style.display = 'block';
                    $("#face_img").attr('src', face_img = "/face_img?t=" + new Date().getTime())
                }
            }
            if ("add_face_submit" == sent_cmd) {
                $("#face_name").val("");
                document.getElementById('add_face_div').style.display = 'none';
                // $("#take_photo").attr('class', "disa");
            }

            if (sent_cmd == 'async') {
                $("#async").attr('class', 'btn btn-danger');
                $("#sync").attr('class', 'btn btn-dark');
            }
            if (sent_cmd == 'sync') {
                $("#sync").attr('class', 'btn btn-danger');
                $("#async").attr('class', 'btn btn-dark');
            }
            if (sent_cmd == 'object_detection') {
                $("#is_face_detection").attr("disabled", true);
            }
            if (sent_cmd == 'face_detection') {
                $("#is_face_detection").attr("disabled", false);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#res").text(textStatus + ":" + jqXHR.status + " " + errorThrown);
        });
        return false;
    }
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

});

