""" ref:
https://github.com/ECI-Robotics/opencv_remote_streaming_processing/
"""

import math
import os
import sys
from logging import DEBUG, INFO, basicConfig, getLogger
from timeit import default_timer as timer

import cv2
import numpy as np

logger = getLogger(__name__)

basicConfig(
    level=INFO,
    format="%(asctime)s %(levelname)s %(name)s %(funcName)s(): %(message)s")

resize_prop = (640, 480)


class VideoCamera(object):
    def __init__(self, input, detections, no_v4l, display):
        if input == 'cam':
            self.input_stream = 0
            if no_v4l:
                self.cap = cv2.VideoCapture(self.input_stream)
            else:
                # for Picamera, added VideoCaptureAPIs(cv2.CAP_V4L)
                try:
                    self.cap = cv2.VideoCapture(self.input_stream, cv2.CAP_V4L)
                except:
                    import traceback
                    traceback.print_exc()
                    print(
                        "\nPlease try to start with command line parameters using --no_v4l\n"
                    )
                    os._exit(0)
        else:
            self.input_stream = input
            assert os.path.isfile(input), "Specified input file doesn't exist"
            self.cap = cv2.VideoCapture(self.input_stream)

        ret, self.frame = self.cap.read()
        # self.frame_ = self.frame.copy()
        cap_prop = self._get_cap_prop()
        logger.info("cap_pop:{}, frame_prop:{}".format(cap_prop, resize_prop))

        self.detections = detections
        self.display = display

    def __del__(self):
        self.cap.release()

    def _get_cap_prop(self):
        return self.cap.get(cv2.CAP_PROP_FRAME_WIDTH), self.cap.get(
            cv2.CAP_PROP_FRAME_HEIGHT), self.cap.get(cv2.CAP_PROP_FPS)

    def get_frame(self, is_async_mode, flip_code, is_object_detection,
                  is_face_detection, is_age_gender_detection,
                  is_emotions_detection, is_head_pose_detection,
                  is_facial_landmarks_detection, is_facial_recognition):

        if is_async_mode:
            ret, next_frame = self.cap.read()
            if not ret:
                return None
            next_frame = cv2.resize(next_frame, resize_prop)
            if self.input_stream == 0 and flip_code is not None:
                next_frame = cv2.flip(next_frame, int(flip_code))
        else:
            ret, self.frame = self.cap.read()
            if not ret:
                return None
            self.frame = cv2.resize(self.frame, resize_prop)
            next_frame = None
            if self.input_stream == 0 and flip_code is not None:
                self.frame = cv2.flip(self.frame, int(flip_code))

        self.frame_ = self.frame.copy()

        if is_object_detection:
            self.frame = self.detections.object_detection(
                self.frame, next_frame, is_async_mode)

        if is_face_detection:
            self.frame = self.detections.face_detection(
                self.frame, next_frame, is_async_mode, is_age_gender_detection,
                is_emotions_detection, is_head_pose_detection,
                is_facial_landmarks_detection, is_facial_recognition)

        if self.display:
            ret, jpeg = cv2.imencode('.jpg', self.frame)
            ret_, jpeg_ = cv2.imencode('.jpg', self.frame_)

        if is_async_mode:
            self.frame = next_frame

        if self.display:
            return jpeg.tostring(), jpeg_.tostring()
        return None

    def take_photo(self):
        # filename = "temp.jpg"
        # filepath = "./static/tmp/"+filename
        # if not os.path.isdir('./static/tmp'):
        #     os.makedirs('./tmp',755)
        # cv2.imwrite(filepath, self.frame_)
        ret, jpeg = cv2.imencode('.jpg', self.frame_)
        return jpeg.tostring()
