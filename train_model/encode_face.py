import argparse
# import json
import os
import pickle
from logging import DEBUG, INFO, basicConfig, getLogger

import cv2
import face_recognition
from imutils import paths

logger = getLogger(__name__)
basicConfig(
    level=INFO,
    format="%(asctime)s [%(levelname)s] %(message)s")

# construct the  argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,
                help="path to input directory of faces + images")
ap.add_argument("-e", "--encodings", required=True,
                help="path to serialized db of facial encodings")
ap.add_argument("-d", "--detection-method", type=str, default="hog",
                help="face detection model to use(default: hog): either `hog` or `cnn`")
args = vars(ap.parse_args())

# grad the paths to the input images in our dataset
logger.info("quantifying faces...")
imagePaths = list(paths.list_images(args["dataset"]))

# initialize the list of known encodings and known names
knownEndcodings = []
knownNames = []

logger.info("serializing encodings...")
# #loop over the image paths
#--------------------the dir's name as person's name means putting lots of images in one dir---------------#
# for (i, imagePath) in enumerate(imagePaths):
#     # extract the person name from the image path
#     logger.info("processing image {}\{}".format(i+1, len(imagePaths)))
#     name = imagePath.split(os.path.sep)[-2]
#     print(name)
#     # load the input image and convert it from BGR (OpenCV ordering)
#     # to dlib ordering (RGB)
#     image = cv2.imread(imagePath)
#     # rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

#     # Resize frame of video to 1/4 size for faster face recognition processing
#     small_frame = cv2.resize(image, (0, 0), fx=0.25, fy=0.25)
#     rgb_small_frame = small_frame[:, :, ::-1]

#     # detect the (x, y)-coordinates of the bounding boxes
#     # corresponding to each face in the input image
#     boxes = face_recognition.face_locations(rgb_small_frame,number_of_times_to_upsample=2,model=args["detection_method"])
#     # print(boxes)
#     # compute the facial embedding for the face
#     encodings = face_recognition.face_encodings(rgb_small_frame, known_face_locations=boxes, num_jitters=200)
  
#     # loop over the encodings
#     for encoding in encodings:
#         # # add each encoding + name to our set of known names and 
#         # # encodings 
#         knownEndcodings.append(encoding)
#         knownNames.append(name)

#--------------------All of the images in one dir---------------#
for (i, imagePath) in enumerate(imagePaths):
    logger.info("processing image {}\{}".format(i+1, len(imagePaths)))
    basename = os.path.splitext(os.path.basename(imagePath))[0]
    # img = face_recognition.load_image_file(imagePath)
    # encodings = face_recognition.face_encodings(img)
    image = cv2.imread(imagePath)
    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(image, (0, 0), fx=0.25, fy=0.25)
    rgb_small_frame = small_frame[:, :, ::-1]
    boxes = face_recognition.face_locations(rgb_small_frame,number_of_times_to_upsample=2)
    encodings = face_recognition.face_encodings(image, boxes, num_jitters=200)

    if len(encodings) > 1:
        print("WARNING: More than one face found in {}. Only considering the first face.".format(imagePath))
        
    if len(encodings) == 0:
        print("WARNING: No faces found in {}. Ignoring file.".format(imagePath))
    else:
        knownNames.append(basename)
        knownEndcodings.append(encodings[0])

    # dump the facial encodings + names to disk
    data = {"encodings": knownEndcodings, "names": knownNames}
    f = open(args["encodings"], "wb")
    f.write(pickle.dumps(data))
    # f.write(json.dumps(data))
    f.close()

logger.info("Encode compelete...")
