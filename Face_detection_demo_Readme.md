## Face Detection Demo 

### 注意事項

- #### 啟動方式：

  1. 開啟docker

     - `docker start e68`
     - `docker attach e68`
     - ![1554891687800](C:\Users\jerrywang\AppData\Roaming\Typora\typora-user-images\1554891687800.png)
  2. 執行程式
     - 移至資料夾路徑
       - `cd ~/c01/openvino/FaceRecognition`
     - Run (二選一)
       - script
         - `./run.sh`	
       - command
         - `python app.py -i cam -l extension/libcpu_extension.so -m_fr dataset --display`
         - ![1554891958357](C:\Users\jerrywang\AppData\Roaming\Typora\typora-user-images\1554891958357.png)
  3. 鏈接網址
     - http://localhost:5000


  
