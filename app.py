###############################################################################
#
# The MIT License (MIT)
#
# Copyright (c) 2014 Miguel Grinberg
#
# Released under the MIT license
# https://github.com/miguelgrinberg/flask-video-streaming/blob/master/LICENSE
#
###############################################################################

# Usage
# for windows
# python app.py -i cam -l extension\cpu_extension.dll -m_fr train_model\models\knn_model.clf --display --no_v4l

import json
import os
import sys
import urllib.request as urllib
from base64 import b64encode
from io import BytesIO
from logging import DEBUG, INFO, basicConfig, getLogger

from flask import (Flask, Response, jsonify, redirect, render_template,
                   request, url_for, flash)
from PIL import Image

import interactive_detection
from camera import VideoCamera

app = Flask(__name__)
logger = getLogger(__name__)
# app.config['UPLOAD_FOLDER'] = './tmp'

basicConfig(
    level=INFO,
    format="%(asctime)s %(levelname)s %(name)s %(funcName)s(): %(message)s")

is_async_mode = True
is_object_detection = False
is_face_detection = True
is_age_gender_detection = False
is_emotions_detection = False
is_head_pose_detection = False
is_facial_landmarks_detection = False
is_facial_recognition = False
flip_code = None  # filpcode: 0,x-axis 1,y-axis -1,both axis

frame_count = 0
frame_origin = None
frame_photo = None
camera = None


def gen(camera, photo=False):
    global frame_count
    global frame_origin
    while True:
        # search face in every 15 frames to speed up process.
        # frame_count += 1
        # if frame_count % 15 == 0:
        # if detections.speech_text == "":
        # return render_template(
        #     'index.html',
        #     is_async_mode=is_async_mode,
        #     flip_code=flip_code,
        #     is_object_detection=is_object_detection,
        #     devices=devices,
        #     models=models,
        #     speech_text=detections.speech_text)
        print(detections.speech_text)
        # flash(detections.speech_text)
        # redirect(url_for('index'))
        frame, frame_origin = camera.get_frame(is_async_mode, flip_code,
                                               is_object_detection,
                                               is_face_detection, is_age_gender_detection,
                                               is_emotions_detection, is_head_pose_detection,
                                               is_facial_landmarks_detection,
                                               is_facial_recognition)
        # frame_photo = frame_origin

        if frame is not None:
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/')
def index():
    return render_template(
        'index.html',
        is_async_mode=is_async_mode,
        flip_code=flip_code,
        is_object_detection=is_object_detection,
        devices=devices,
        models=models)


@app.route('/video_feed')
def video_feed():
    global camera
    # crash on mutli-window
    # camera = VideoCamera(args.input, detections, args.no_v4l, args.display)
    return Response(
        gen(camera), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/detection', methods=['POST'])
# communicate with the front-end
def detection():
    global is_async_mode
    global is_object_detection
    global is_face_detection
    global is_age_gender_detection
    global is_emotions_detection
    global is_head_pose_detection
    global is_facial_landmarks_detection
    global is_facial_recognition

    command = request.json['command']
    if command == "async":
        is_async_mode = True
    elif command == "sync":
        is_async_mode = False

    if command == "object_detection":
        is_object_detection = True
        is_face_detection = False
    if command == "face_detection":
        is_face_detection = True
        is_object_detection = False
    if command == "age_gender_detection" and not is_object_detection:
        is_age_gender_detection = not is_age_gender_detection
    if command == "emotions_detection" and not is_object_detection:
        is_emotions_detection = not is_emotions_detection
    if command == "head_pose_detection" and not is_object_detection:
        is_head_pose_detection = not is_head_pose_detection
    if command == "facial_landmarks_detection" and not is_object_detection:
        is_facial_landmarks_detection = not is_facial_landmarks_detection
    if command == "facial_recognition" and not is_object_detection:
        is_facial_recognition = not is_facial_recognition
        print("is_facial_recognition: ", is_facial_recognition)

    result = {
        "command": "is_async_mode",
        "is_async_mode": is_async_mode,
        "flip_code": flip_code,
        "is_object_detection": is_object_detection,
        "is_face_detection": is_face_detection,
        "is_age_gender_detection": is_age_gender_detection,
        "is_emotions_detection": is_emotions_detection,
        "is_head_pose_detection": is_head_pose_detection,
        "is_facial_landmarks_detection": is_facial_landmarks_detection,
        "is_facial_recognition": is_facial_recognition
    }
    logger.info(
        "command:{} is_async:{} flip_code:{} is_obj_det:{} is_face_det:{} is_ag_det:{} is_em_det:{} is_hp_det:{} is_lm_det:{} is_fr:{}".
        format(command, is_async_mode, flip_code, is_object_detection,
               is_face_detection, is_age_gender_detection,
               is_emotions_detection, is_head_pose_detection,
               is_facial_landmarks_detection, is_facial_recognition))
    return jsonify(ResultSet=json.dumps(result))


@app.route('/flip', methods=['POST'])
def flip_frame():
    global flip_code

    command = request.json['command']

    if command == "flip" and flip_code is None:
        flip_code = 0
    elif command == "flip" and flip_code == 0:
        flip_code = 1
    elif command == "flip" and flip_code == 1:
        flip_code = -1
    elif command == "flip" and flip_code == -1:
        flip_code = None

    result = {
        "command": "is_async_mode",
        "is_async_mode": is_async_mode,
        "flip_code": flip_code,
        "is_object_detection": is_object_detection,
        "is_face_detection": is_face_detection,
        "is_age_gender_detection": is_age_gender_detection,
        "is_emotions_detection": is_emotions_detection,
        "is_head_pose_detection": is_head_pose_detection,
        "is_facial_landmarks_detection": is_facial_landmarks_detection,
        "is_facial_recognition": is_facial_recognition
    }
    logger.info(
        "command:{} is_async:{} flip_code:{} is_obj_det:{} is_face_det:{} is_ag_det:{} is_em_det:{} is_hp_det:{} is_lm_det:{} is_fr:{}".
        format(command, is_async_mode, flip_code, is_object_detection,
               is_face_detection, is_age_gender_detection,
               is_emotions_detection, is_head_pose_detection,
               is_facial_landmarks_detection, is_facial_recognition))
    return jsonify(ResultSet=json.dumps(result))


@app.route('/face_img')
def face_img(face_img_key=0):
    global frame_photo, frame_origin
    global camera
    # frame_photo = camera.take_photo()
    frame_photo = frame_origin
    if frame_origin is not None:
        frame_tmp = (b'--frame\r\n'
                     b'Content-Type: image/jpeg\r\n\r\n' + frame_origin + b'\r\n\r\n')
    return Response(frame_tmp, mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/add_face', methods=['POST'])
# @app.route('/takephoto')
def add_face():
    global frame_photo
    global camera
    command = request.json['command']
    face_name = request.json['face_name']
    result = None
    filepath = None
    if command == "take_photo":
        result = "take_photo"

        # file_base64 = b64encode(BytesIO(camera.take_photo()).getvalue())
        # return redirect('index.html', face_img_data=file_base64)
        # frame_photo = camera.take_photo()
        # face_output = (b'--frame\r\n'
        #             b'Content-Type: image/jpeg\r\n\r\n' + frame_photo + b'\r\n\r\n')
        # redirect(url_for('face_img'))

    if command == "add_face_submit":
        result = "add_face_submit"
        image = Image.open(BytesIO(frame_photo))
        image.save("./dataset/"+face_name+".jpg")
        # re-load model
        detections.scan_known_people(
            detections.models[6])
    logger.info("{}".format(result))
    result = {
        "command": result,
        "face_count": detections.get_face_count()
    }
    return jsonify(ResultSet=json.dumps(result))


if __name__ == '__main__':

    # arg parse
    args = interactive_detection.build_argparser().parse_args()
    devices = [
        args.device, args.device, args.device_age_gender, args.device_emotions,
        args.device_head_pose, args.device_facial_landmarks, args.device_facial_recognition
    ]
    models = [
        args.model_ssd, args.model_face, args.model_age_gender,
        args.model_emotions, args.model_head_pose, args.model_facial_landmarks, args.model_facial_recognition
    ]
    if "CPU" in devices :
        logger.warn(
            "\nPlease try to specify cpu extensions library path in command line parameters using -l "
            "or --cpu_extension command line argument")
        # sys.exit(1)

    # Create detectors class instance
    detections = interactive_detection.Detections(
        devices, models, args.cpu_extension, args.plugin_dir,
        args.prob_threshold, args.prob_threshold_face, is_async_mode)
    models = detections.models  # Get models to display WebUI.

    camera = VideoCamera(args.input, detections, args.no_v4l, args.display)

    app.run(host='0.0.0.0', threaded=True)
