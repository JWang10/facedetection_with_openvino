import os
import re
import sys

import psycopg2

# use PostgreSQL


class SQL():
    def __init__(self):
        # self.conn = psycopg2.connect(
        #     database="face", user="c01", password="c01", host="127.0.0.1", port="5231")
        self.conn = None
        self.cursor = None

    def connectSQL(self):
        self.conn = psycopg2.connect(
            database="face", user="c01", password="c01", host="192.168.31.107", port="5231")
        self.cursor = self.conn.cursor()
        print('connect successful!')

    def closeSQL(self):
        self.conn.commit()
        self.conn.close()

    def createTable(self):
        # conn = psycopg2.connect(database="face", user="c01",
        #                         password="c01", host="127.0.0.1", port="5231")
        self.connectSQL()
        cursor.execute('''create table faces(
        face_name varchar(32) not null,
        face_encoding varchar(128) not null
        )''')
        self.closeSQL()

    def addFace(self, known_names, known_face_encodings):
        # self.connectSQL()
        query = "INSERT INTO faces (face_name, face_encoding_low, face_encoding_high) VALUES ('{}', CUBE(array[{}]), CUBE(array[{}])) \
         ON CONFLICT (face_name) DO UPDATE SET face_encoding_low=Excluded.face_encoding_low, face_encoding_high=Excluded.face_encoding_high".format(
                known_names,
                ','.join(str(s) for s in known_face_encodings[0:64]),
                ','.join(str(s) for s in known_face_encodings[64:128]),
        )
        self.cursor.execute(query)
        # self.closeSQL()

    def searchFace(self, face_encodings, threshold=0.5):
        # self.connectSQL()
        query = "SELECT face_name, sqrt(power(CUBE(array[{}]) <-> face_encoding_low, 2) + power(CUBE(array[{}]) <-> face_encoding_high, 2)) as dist FROM faces ORDER BY dist ASC LIMIT 1".format(
                ','.join(str(s) for s in face_encodings[0][0:64]),
                ','.join(str(s) for s in face_encodings[0][64:128]),
        )
        self.cursor.execute(query)
        row = self.cursor.fetchone()
        face_name = "Unknown"
        while row is not None:
            if row[1] < threshold:
                face_name = row[0]
            break
        return face_name
